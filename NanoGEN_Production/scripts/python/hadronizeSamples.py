from pathlib import Path
import argparse
import os
import sys
import subprocess
import logging
from concurrent.futures import ThreadPoolExecutor, wait

# Set environment variables
os.environ['EOS_MGM_URL'] = 'root://lyoeos.in2p3.fr'
os.environ['X509_USER_PROXY'] = os.path.expanduser('~/x509up_u2395')
os.environ['EOSHOME'] = f'/eos/lyoeos.in2p3.fr/home/{os.getlogin()}'

def convert_files(lheFile, configFile, edmFile, logFile, nanoGEN_File):
    """
    Convert LHE file to EDM file using the converter.sh script.
    Args:
        lheFile (Path): Path to the LHE file.
        configFile (Path): Path to the config file.
        edmFile (Path): Path to the EDM file.
        logFile (Path): Path to the log file.
        nanoGEN_File (Path): Path to the nanoGEN file.
    """
    command = f'srun --job-name={lheFile.name} converter.sh {configFile} {edmFile} {lheFile} {nanoGEN_File}'
    with open(logFile, 'w') as log:
        process = subprocess.Popen(command, stdout=log, stderr=log, shell=True)
        process.wait()

def check_directory(directory_path):
    """
    Check if a directory exists, and create it if it doesn't.
    Args:
        directory_path (Path): Path to the directory.
    """
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
        print(f"Directory '{directory_path}' created.")
    else:
        print(f"Directory '{directory_path}' already exists.")

# Parse command-line arguments
parser = argparse.ArgumentParser(description='Process files in a directory.')
parser.add_argument('input_directory', type=str, help='Path to the input directory. The directory that holds the folders containing the LHE files, config files, etc...')
parser.add_argument('--limit', type=int, help='Limit for testing purposes.')
parser.add_argument('--batch_size', type=int, default=100, help='Batch size for parallel processing.')
parser.add_argument('--verbose', action='store_true', help='Enable verbose logging.')
args = parser.parse_args()

input_directory = args.input_directory
limit = args.limit
batch_size = args.batch_size
verbose = args.verbose

path = Path(input_directory)

# Checks if ConfigFiles, edmFiles and nanoGEN directories exist
path_configFiles = path / 'configFiles'
path_edmFiles = path / 'edmFiles'
path_nanoGEN = path / 'nanoGEN'
path_logFiles = path / 'logFiles'

check_directory(path_configFiles)
check_directory(path_edmFiles)
check_directory(path_nanoGEN)
check_directory(path_logFiles)

# Get LHE files in a list
lheFiles = []
configFiles = []
edmFiles = []
logFiles = []
nanoGEN_Files = []

for lheFile in path.rglob('*.lhe'):
    lheFiles.append(lheFile)
    configFiles.append(path_configFiles / lheFile.name.replace('.lhe', '.py'))
    edmFiles.append(path_edmFiles / lheFile.name.replace('.lhe', '.root'))
    logFiles.append(path_logFiles / lheFile.name.replace('.lhe', '.log'))
    nanoGEN_Files.append(path_nanoGEN / lheFile.name.replace('.lhe', '_NanoGEN.root'))

# Check if the lists have the same size
if len(lheFiles) != len(configFiles) or len(lheFiles) != len(edmFiles) or len(lheFiles) != len(logFiles) or len(lheFiles) != len(nanoGEN_Files):
    print("Lists have different sizes. Exiting the program.")
    sys.exit(1)

num_jobs = min(len(lheFiles), limit)  # Limit the number of jobs based on the provided limit
num_batches = (num_jobs + batch_size - 1) // batch_size

if verbose:
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

with ThreadPoolExecutor(max_workers=batch_size) as executor:
    job_queue = []
    for i in range(num_jobs):
        lheFile = lheFiles[i]
        configFile = configFiles[i]
        edmFile = edmFiles[i]
        logFile = logFiles[i]
        nanoGEN_File = nanoGEN_Files[i]

        job_queue.append(executor.submit(convert_files, lheFile, configFile, edmFile, logFile, nanoGEN_File))

        if verbose:
            logging.info(f"Processing file {lheFile.name}")

        if len(job_queue) == batch_size or i == num_jobs - 1:
            job_queue = []  # Clear the job queue for the next batch
