# Script: skimmer.py
# Author: Christopher Greenberg
# Contact: c.greenberg@cer.ch

import argparse
from pathlib import Path
import ROOT
from multiprocessing import Pool
import os
import logging


def get_root_file_list(input_folder, limit=0):
    in_folder = Path(input_folder)
    file_list = list(in_folder.glob('*.root'))  # Get all files in the folder
    if limit > 0:
        file_list = file_list[:limit]  # Limit the file list to the specified number
    return file_list


def process_file(file, output_folder, logger):
    logger.info(f'Skimming: {file.name}')

    # Check if the output file already exists
    outputFile = file.name.replace('.root', '_skimmed.root')
    outputPath = output_folder / outputFile
    if os.path.exists(outputPath):
        logger.info(f'{outputPath} already exists. Skipping file.')
        return

    try:
        inputFile = ROOT.TFile(str(file), "READ")
        inputTree = inputFile.Get("Events")

        inputTree.SetBranchStatus("*", 0)
        inputTree.SetBranchStatus("*GenJet*", 1)
        inputTree.SetBranchStatus("*LHE*", 1)
        inputTree.SetBranchStatus("*GenDressedLepton*", 1)
        inputTree.SetBranchStatus("*MET_fiducial*", 1)
        inputTree.SetBranchStatus("Generator_weight", 1)
        inputTree.SetBranchStatus("genWeight", 1)

        outputTFile = ROOT.TFile(str(outputPath), "RECREATE")
        outputTTree = inputTree.CloneTree()

        outputTFile.cd()
        outputTTree.Write()
        outputTFile.Close()
    except Exception as e:
        logger.error(f"Error processing file {file.name}: {str(e)}")


def process_files_parallel(file_list, output_folder, logger):
    pool = Pool()
    pool.starmap(process_file, [(file, output_folder, logger) for file in file_list])
    pool.close()
    pool.join()


def create_output_folder(output_folder):
    # Create the output folder if it doesn't exist
    if not output_folder.exists():
        logger = logging.getLogger(__name__)
        logger.info(f'Creating folder for output in {output_folder}')
        output_folder.mkdir(parents=True, exist_ok=True)


def configure_logging(verbose=False):
    log_level = logging.DEBUG if verbose else logging.INFO
    logging.basicConfig(level=log_level, format='%(levelname)s: %(message)s')
    return logging.getLogger(__name__)


def main():
    # Create the argument parser
    parser = argparse.ArgumentParser(description='Process files in a folder')

    # Add the positional argument for the folder path
    parser.add_argument('--input_folder', type=str, help='Path to input folder')
    parser.add_argument('--output_folder', type=str, help='Path to output folder')
    parser.add_argument('--limit', type=int, default=0, help='Limit the number of files to process')
    parser.add_argument('--verbose', action='store_true', help='Enable verbose output')

    # Parse the arguments
    args = parser.parse_args()

    # Configure logging
    logger = configure_logging(args.verbose)

    # Access the folder path
    input_folder = args.input_folder
    output_folder = Path(args.output_folder)

    # Get the list of files in the folder (limit files by setting a number different than zero here)
    file_list = get_root_file_list(input_folder, limit=args.limit)

    # Create the output folder if it doesn't exist
    create_output_folder(output_folder)

    # Parallelize the file processing
    process_files_parallel(file_list, output_folder, logger)


if __name__ == '__main__':
    main()
