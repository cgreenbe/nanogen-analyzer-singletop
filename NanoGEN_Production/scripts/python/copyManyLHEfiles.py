import os
import shutil
import argparse
from multiprocessing import Pool

def process_file(file_path, subfolder_name, target_folder):
    filename = os.path.basename(file_path)

    # Skip files with the .root extension
    if '.root' in filename:
        return

    # Check if the file is gzipped and contains 'unweighted_events' in the filename
    if 'unweighted_events' in filename:
        if filename.endswith('.gz'):
            # Gunzip the file if it is compressed
            gunzipped_filename = subfolder_name + '_' + filename[:-3]
            print(f"gunzip and copy {file_path}")
            os.system(f"gunzip {file_path}")
        else:
            gunzipped_filename = subfolder_name + '_' + filename

        # Check if the gunzipped file already exists in the target folder
        target_file_path = os.path.join(target_folder, gunzipped_filename)
        if os.path.exists(target_file_path):
            print(f'{gunzipped_filename} already exists in the target folder. Skipping...')
            return

        # Copy the gunzipped file to the target folder
        if filename.endswith('.gz'):
            shutil.copy(file_path[:-3], target_file_path)
        else:
            shutil.copy(file_path, target_file_path)


def gunzip_and_copy_files(source_folder, target_folder, target_run):
    # Check if the target run name ends with an underscore
    if target_run[-1] != '_':
        print(f'Careful with target run name! Check it again: {target_run}')
        print('Killing the process')
        exit(1)

    # Get a list of subfolders that start with the target run name and exclude .txt files
    subfolders = [subfolder for subfolder in os.listdir(source_folder) if subfolder.startswith(target_run) and '.txt' not in subfolder]
    print(f'Number of subfolders: {len(subfolders)}')

    # Create a pool of worker processes
    pool = Pool()

    # Iterate over the subfolders
    for subfolder_name in subfolders:
        subfolder_path = os.path.join(source_folder, subfolder_name)

        # Check if the subfolder exists
        if os.path.isdir(subfolder_path):
            # Iterate over the files in the subfolder and process them in parallel
            file_paths = [os.path.join(subfolder_path, filename) for filename in os.listdir(subfolder_path)]
            pool.starmap(process_file, [(file_path, subfolder_name, target_folder) for file_path in file_paths])

    # Close the pool to free resources
    pool.close()
    pool.join()


if __name__ == '__main__':
    # Parse command-line arguments
    parser = argparse.ArgumentParser(description='Gunzip and copy files from subfolders.')
    parser.add_argument('source_folder', type=str, help='Path to the source folder (the path to the Events folder of MG5. The name of the run will be given as the third argument)')
    parser.add_argument('target_folder', type=str, help='Path to the target folder')
    parser.add_argument('target_run', type=str, help='Give the Run to be copied')
    args = parser.parse_args()

    # Retrieve the parsed arguments
    source_folder = args.source_folder
    target_folder = args.target_folder
    target_run = args.target_run

    # Call the main function to perform the file operations
    gunzip_and_copy_files(source_folder, target_folder, target_run)
