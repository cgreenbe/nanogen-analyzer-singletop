#!/bin/bash

export EOS_MGM_URL=root://lyoeos.in2p3.fr;
export X509_USER_PROXY=$HOME/x509up_u2395;
export EOSHOME=/eos/lyoeos.in2p3.fr/home/$USER;

cmsDriver.py  --python_filename $1 --eventcontent LHE --datatier LHE --fileout file:$2 --conditions auto:mc --step NONE --filein file:$3 --mc -n -1

cmsDriver.py Configuration/GenProduction/python/ThirteenTeV/Hadronizer/Hadronizer_TuneCP5_13TeV_generic_LHE_pythia8_PSweights_cff.py --filein file:$2 \
    --fileout file:$4 --mc --eventcontent NANOAODGEN --datatier NANOAODSIM \
    --conditions auto:mc --step GEN,NANOGEN --nThreads 4 --python_filename $1 \
    -n -1


# $1 = ConfigFile
# $2 = EDMFile
# $3 = LHEFile
# $4 = NanoGENFile